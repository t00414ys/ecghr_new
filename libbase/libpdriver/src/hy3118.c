/*!******************************************************************************
 * @file    hy3118.c
 * @brief   hy3118 sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "timer.h"
#include "hy3118.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_HY3118


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(2)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version


struct {
	unsigned char		addr;
	unsigned char		buff_ch1[3];	// transmission buffer
	unsigned char		buff_ch2[3];	// transmission buffer
	unsigned char		buff_ch3[3];	// transmission buffer

	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[4];
	//
	unsigned char		read_count;
} g_hy3118;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

int adc_hy3118_enable_channel( int channel_no )
{
	unsigned char		buff;

	//For ignore first three data
	g_hy3118.read_count = 0;

	//ENLDO and ENREFO
	buff = ENLDO | ENREFO;
	i2c_write( g_hy3118.addr, HY311x_SYS, &buff, 1 );

	////It have to delay 1ms after ENLDO and ENREFO.
	//mdelay(1);

	//Enable channel
	if( channel_no == 1 ) {
		//INP:AIN1   INN:VSSA
		buff = INP_AIN1 | INN_VSSA;
		i2c_write( g_hy3118.addr, HY311x_ADC1, &buff, 1 );
	}
#if defined(HY31118_ENABLE_CHN2)
	else if( channel_no == 2 ) {
		//INP:AIN2   INN:VSSA
		buff = INP_AIN2 | INN_VSSA;
		i2c_write( g_hy3118.addr, HY311x_ADC1, &buff, 1 );
	}
#endif
#if defined(HY31118_ENABLE_CHN3)
	else if( channel_no == 3 ) {
		//INP:AIN3   INN:VSSA
		buff = INP_AIN3 | INN_VSSA;
		i2c_write( g_hy3118.addr, HY311x_ADC1, &buff, 1 );
	}
#endif

	//ENADC
	buff = ENLDO | ENREFO | ENADC;
	i2c_write( g_hy3118.addr, HY311x_SYS, &buff, 1 );

	return RESULT_SUCCESS_INIT;
}

int adc_hy3118_disable_channel( void )
{
	unsigned char		buff;

	buff = 0;
	i2c_read( g_hy3118.addr, HY311x_SYS, &buff, 1 );
	buff = buff ^ ( ENLDO | ENREFO | ENADC );
	i2c_write( g_hy3118.addr, HY311x_SYS, &buff, 1 );

	return RESULT_SUCCESS_INIT;
}

int adc_hy3118_init( unsigned int param )
{
	int ret;
	unsigned char buff;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	//Initial
	g_hy3118.addr = HY3118_I2C_ADDRESS;

	buff = ENLDO | ENREFO;
	ret = i2c_write( g_hy3118.addr, HY311x_SYS, &buff, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	//OSCS:1000KHz   FRb:1/2(1.5V)	FRb_Full(2.8V)
	buff = OSCS_Int1000KHz | FRb_Full;
	i2c_write( g_hy3118.addr, HY311x_ADC3, &buff, 1 );

	//LDO:3.0V   REFO:1.5V   HS:High(1000K)   OSR:128(7680Hz)
	buff = LDO_3p0 | REFO_1p5 | HS_High | OSR_128;
	i2c_write( g_hy3118.addr, HY311x_ADC4, &buff, 1 );

	//Initial finished. Switch to power down mode
	adc_hy3118_disable_channel();

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void adc_hy3118_ctrl( int f_ena )
{
	if( f_ena ) {
		adc_hy3118_enable_channel( 1 ); // default is to enable channel1
	} else {
		adc_hy3118_disable_channel(); // Switch to power down mode
	}
}


unsigned int adc_hy3118_rcv( unsigned int tick )
{
	int		recv_ch1 = 0, recv_ch2 = 0, recv_ch3 = 0;

	//Channel 1
	adc_hy3118_enable_channel( 1 );

	while( 1 ) {
		recv_ch1 = i2c_read( g_hy3118.addr, HY311x_ADO, g_hy3118.buff_ch1, 3 );
		if( recv_ch1 == 0 ) {
			g_hy3118.read_count++;
			if( g_hy3118.read_count >= 4 ) {
				if( g_hy3118.buff_ch1[2] & 0x01 ) {
					break;
				}
			}
		}
	}

#if defined(HY31118_ENABLE_CHN2)
	//Channel 2
	adc_hy3118_enable_channel( 2 );

	while( 1 ) {
		recv_ch2 = i2c_read( g_hy3118.addr, HY311x_ADO, g_hy3118.buff_ch2, 3 );
		if( recv_ch2 == 0 ) {
			g_hy3118.read_count++;
			if( g_hy3118.read_count >= 4 ) {
				if( g_hy3118.buff_ch2[2] & 0x01 ) {
					break;
				}
			}
		}
	}
#endif

#if defined(HY31118_ENABLE_CHN3)
	//Channel 3
	adc_hy3118_enable_channel( 3 );

	while( 1 ) {
		recv_ch3 = i2c_read( g_hy3118.addr, HY311x_ADO, g_hy3118.buff_ch3, 3 );
		if( recv_ch3 == 0 ) {
			g_hy3118.read_count++;
			if( g_hy3118.read_count >= 4 ) {
				if( g_hy3118.buff_ch3[2] & 0x01 ) {
					break;
				}
			}
		}
	}
#endif

	g_hy3118.recv_result = recv_ch1 + recv_ch2 + recv_ch3;
	if( g_hy3118.recv_result == 0 ) {
		g_hy3118.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_hy3118.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}

	return 0;
}

/*
 * HY3118 data is 23bits. Based on sign extension issue, if you want to shift bits, please modify the DEFINITION
 */

#define	BITS_SHIFT	12

int adc_hy3118_conv( frizz_fp data[4] )
{
	int					*fp = ( int* )( void* )data;
	unsigned char data_ready = 0, data_ready_ch1 = 0, data_ready_ch2 = 0, data_ready_ch3 = 0;

	if( g_hy3118.recv_result != 0 ) {
		data[0] = g_hy3118.lasttime_data[0];
		data[1] = g_hy3118.lasttime_data[1];
		data[2] = g_hy3118.lasttime_data[2];
		data[3] = g_hy3118.lasttime_data[3];
		return RESULT_SUCCESS_CONV;
	}

	data_ready_ch1 = g_hy3118.buff_ch1[2] & 0x01;
	fp[0] = ( int )( ( ( ( ( g_hy3118.buff_ch1[0] << 16 ) | ( g_hy3118.buff_ch1[1] << 8 ) ) | g_hy3118.buff_ch1[2] ) << 8 ) >> ( 9 + BITS_SHIFT ) );

#if defined(HY31118_ENABLE_CHN2)
	data_ready_ch2 = g_hy3118.buff_ch2[2] & 0x01;
	fp[1] = ( int )( ( ( ( ( g_hy3118.buff_ch2[0] << 16 ) | ( g_hy3118.buff_ch2[1] << 8 ) ) | g_hy3118.buff_ch2[2] ) << 8 ) >> ( 9 + BITS_SHIFT ) );
#endif

#if defined(HY31118_ENABLE_CHN3)
	data_ready_ch3 = g_hy3118.buff_ch3[2] & 0x01;
	fp[2] = ( int )( ( ( ( ( g_hy3118.buff_ch3[0] << 16 ) | ( g_hy3118.buff_ch3[1] << 8 ) ) | g_hy3118.buff_ch3[2] ) << 8 ) >> ( 9 + BITS_SHIFT ) );
#endif

	g_hy3118.lasttime_data[0] = data[0];
	g_hy3118.lasttime_data[1] = data[1];
	g_hy3118.lasttime_data[2] = data[2];
	g_hy3118.lasttime_data[3] = data[3];

	data_ready = data_ready_ch1 + data_ready_ch2 + data_ready_ch3;
#if defined(HY31118_ENABLE_CHN3)
	if( data_ready == 3 )	{
#elif defined(HY31118_ENABLE_CHN2)
	if( data_ready == 2 )	{
#else
	if( data_ready == 1 )	{
#endif
		return RESULT_SUCCESS_CONV;
	} else {
		return RESULT_ERR_CONV;
	}
}

int adc_hy3118_setparam( void *ptr )
{
	return RESULT_ERR_SET;
}

int adc_hy3118_get_condition( void *data )
{
	return g_hy3118.device_condition;
}

unsigned int adc_hy3118_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int adc_hy3118_get_name()
{
	return	D_DRIVER_NAME;
}
