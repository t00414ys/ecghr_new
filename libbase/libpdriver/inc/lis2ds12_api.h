/*!******************************************************************************
 * @file    lis2ds12_api.h
 * @brief   lis2ds12 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __LIS2DS12_API_H__
#define __LIS2DS12_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int lis2ds12_init( unsigned int param );
void lis2ds12_ctrl_accl( int f_ena );
unsigned int lis2ds12_rcv_accl( unsigned int tick );
int lis2ds12_conv_accl( frizz_fp *data );
int lis2ds12_setparam_accl( void *ptr );
unsigned int lis2ds12_get_ver( void );
unsigned int lis2ds12_get_name( void );

int lis2ds12_accl_get_condition( void *data );
int lis2ds12_accl_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif

#endif
