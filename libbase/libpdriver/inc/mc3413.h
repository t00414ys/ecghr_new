/*!******************************************************************************
 * @file    mc3413.h
 * @brief   mc3413 sensor driver header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 * @par     Data Sheet
 *          MC3413 3-Axis Accelerometer Data Sheet APS-048-0029v1.7
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MC3413_H__
#define __MC3413_H__
#include "mc3413_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */

/*
 * This is use accel sensor range (To select either one)
 *(DEFINITED) : Enable  use accl sensor range
 *(~DEFINITED): Disable  use accl sensor range
 */
//#define ACC_2G_RANGE				// p37 default
//#define ACC_4G_RANGE
//#define ACC_8G_RANGE
#define ACC_16G_RANGE
//#define ACC_12G_RANGE

/*
 * This is use accel sensor range & coefficient & resolution
 *(DEFINITED) : Enable  use accl sensor resolution
 *(~DEFINITED): Disable  use accl sensor resolution
 */
#if defined(ACC_2G_RANGE)
#define MC3413_ACCL_RANGE				(0x00)
#define MC3413_ACCL_PER_LSB			((float)(2./8192.))		// coefficient of resolution 14bits
#define MC3413_OUTCFG_RAN_RES		(0x05)					// p37 RANGE +-2g  RESOLUTION 14bits  0-000_0-101
//#define MC3413_ACCL_PER_LSB		((float)(2./2048.))		// coefficient of resolution 12bits
//#define MC3413_OUTCFG_RAN_RES		(0x04)					// p37 RANGE +-2g  RESOLUTION 12bits  0-000_0-100
//#define MC3413_ACCL_PER_LSB		((float)(2./512.))		// coefficient of resolution 10bits
//#define MC3413_OUTCFG_RAN_RES		(0x03)					// p37 RANGE +-2g  RESOLUTION 10bits  0-000_0-011
//#define MC3413_ACCL_PER_LSB		((float)(2./128.))		// coefficient of resolution 8bits
//#define MC3413_OUTCFG_RAN_RES		(0x02)					// p37 RANGE +-2g  RESOLUTION 8bits  0-000_0-010
//#define MC3413_ACCL_PER_LSB		((float)(2./64.))		// coefficient of resolution 7bits
//#define MC3413_OUTCFG_RAN_RES		(0x01)					// p37 RANGE +-2g  RESOLUTION 7bits  0-000_0-001
//#define MC3413_ACCL_PER_LSB		((float)(2./32.))		// coefficient of resolution 6bits
//#define MC3413_OUTCFG_RAN_RES		(0x00)					// p37 RANGE +-2g  RESOLUTION 6bits  0-000_0-000

#elif defined(ACC_4G_RANGE)
#define MC3413_ACCL_RANGE				(0x01)
#define MC3413_ACCL_PER_LSB			((float)(4./8192.))		// coefficient of resolution 14bits
#define MC3413_OUTCFG_RAN_RES		(0x15)					// p37 RANGE +-4g  RESOLUTION 14bits  0-001_0-101
//#define MC3413_ACCL_PER_LSB		((float)(4./2048.))		// coefficient of resolution 12bits
//#define MC3413_OUTCFG_RAN_RES		(0x14)					// p37 RANGE +-4g  RESOLUTION 12bits  0-001_0-100
//#define MC3413_ACCL_PER_LSB		((float)(4./512.))		// coefficient of resolution 10bits
//#define MC3413_OUTCFG_RAN_RES		(0x13)					// p37 RANGE +-4g  RESOLUTION 10bits  0-001_0-011
//#define MC3413_ACCL_PER_LSB		((float)(4./128.))		// coefficient of resolution 8bits
//#define MC3413_OUTCFG_RAN_RES		(0x12)					// p37 RANGE +-4g  RESOLUTION 8bits  0-001_0-010
//#define MC3413_ACCL_PER_LSB		((float)(4./64.))		// coefficient of resolution 7bits
//#define MC3413_OUTCFG_RAN_RES		(0x11)					// p37 RANGE +-4g  RESOLUTION 7bits  0-001_0-001
//#define MC3413_ACCL_PER_LSB		((float)(4./32.))		// coefficient of resolution 6bits
//#define MC3413_OUTCFG_RAN_RES		(0x10)					// p37 RANGE +-4g  RESOLUTION 6bits  0-001_0-000

#elif defined(ACC_8G_RANGE)
#define MC3413_ACCL_RANGE				(0x02)
#define MC3413_ACCL_PER_LSB			((float)(8./8192.))		// coefficient of resolution 14bits
#define MC3413_OUTCFG_RAN_RES		(0x25)					// p37 RANGE +-8g  RESOLUTION 14bits  0-010_0-101
//#define MC3413_ACCL_PER_LSB		((float)(8./2048.))		// coefficient of resolution 12bits
//#define MC3413_OUTCFG_RAN_RES		(0x24)					// p37 RANGE +-8g  RESOLUTION 12bits  0-010_0-100
//#define MC3413_ACCL_PER_LSB		((float)(8./512.))		// coefficient of resolution 10bits
//#define MC3413_OUTCFG_RAN_RES		(0x23)					// p37 RANGE +-8g  RESOLUTION 10bits  0-010_0-011
//#define MC3413_ACCL_PER_LSB		((float)(8./128.))		// coefficient of resolution 8bits
//#define MC3413_OUTCFG_RAN_RES		(0x22)					// p37 RANGE +-8g  RESOLUTION 8bits  0-010_0-010
//#define MC3413_ACCL_PER_LSB		((float)(8./64.))		// coefficient of resolution 7bits
//#define MC3413_OUTCFG_RAN_RES		(0x21)					// p37 RANGE +-8g  RESOLUTION 7bits  0-010_0-001
//#define MC3413_ACCL_PER_LSB		((float)(8./32.))		// coefficient of resolution 6bits
//#define MC3413_OUTCFG_RAN_RES		(0x20)					// p37 RANGE +-8g  RESOLUTION 6bits  0-010_0-000

#elif defined(ACC_16G_RANGE)
#define MC3413_ACCL_RANGE				(0x03)
#define MC3413_ACCL_PER_LSB			((float)(16./8192.))	// coefficient of resolution 14bits
#define MC3413_OUTCFG_RAN_RES		(0x35)					// p37 RANGE +-16g  RESOLUTION 14bits  0-011_0-101
//#define MC3413_ACCL_PER_LSB		((float)(16./2048.)		// coefficient of resolution 12bits
//#define MC3413_OUTCFG_RAN_RES		(0x34)					// p37 RANGE +-16g  RESOLUTION 12bits  0-011_0-100
//#define MC3413_ACCL_PER_LSB		((float)(16./512.))		// coefficient of resolution 10bits
//#define MC3413_OUTCFG_RAN_RES		(0x33)					// p37 RANGE +-16g  RESOLUTION 10bits  0-011_0-011
//#define MC3413_ACCL_PER_LSB		((float)(16./128.))		// coefficient of resolution 8bits
//#define MC3413_OUTCFG_RAN_RES		(0x32)					// p37 RANGE +-16g  RESOLUTION 8bits  0-011_0-010
//#define MC3413_ACCL_PER_LSB		((float)(16./64.))		// coefficient of resolution 7bits
//#define MC3413_OUTCFG_RAN_RES		(0x31)					// p37 RANGE +-16g  RESOLUTION 7bits  0-011_0-001
//#define MC3413_ACCL_PER_LSB		((float)(16./32.))		// coefficient of resolution 6bits
//#define MC3413_OUTCFG_RAN_RES		(0x30)					// p37 RANGE +-16g  RESOLUTION 6bits  0-011_0-000

#elif defined(ACC_12G_RANGE)
#define MC3413_ACCL_RANGE				(0x04)
#define MC3413_ACCL_PER_LSB			((float)(12./8192.))	// coefficient of resolution 14bits
#define MC3413_OUTCFG_RAN_RES		(0x45)					// p37 RANGE +-12g  RESOLUTION 14bits  0-100_0-101
//#define MC3413_ACCL_PER_LSB		((float)(12./2048.))	// coefficient of resolution 12bits
//#define MC3413_OUTCFG_RAN_RES		(0x44)					// p37 RANGE +-12g  RESOLUTION 12bits  0-100_0-100
//#define MC3413_ACCL_PER_LSB		((float)(12./512.))		// coefficient of resolution 10bits
//#define MC3413_OUTCFG_RAN_RES		(0x43)					// p37 RANGE +-12g  RESOLUTION 10bits  0-100_0-011
//#define MC3413_ACCL_PER_LSB		((float)(12./128.))		// coefficient of resolution 8bits
//#define MC3413_OUTCFG_RAN_RES		(0x42)					// p37 RANGE +-12g  RESOLUTION 8bits  0-100_0-010
//#define MC3413_ACCL_PER_LSB		((float)(12./64.))		// coefficient of resolution 7bits
//#define MC3413_OUTCFG_RAN_RES		(0x41)					// p37 RANGE +-12g  RESOLUTION 7bits  0-100_0-001
//#define MC3413_ACCL_PER_LSB		((float)(12./32.))		// coefficient of resolution 6bits
//#define MC3413_OUTCFG_RAN_RES		(0x40)					// p37 RANGE +-12g  RESOLUTION 6bits  0-100_0-000
#endif






/*
 *
 *		The rest of this is okay not change
 *
 */
// driver parameter
#define MC3413_I2C_ADDRESS_L			(0x4C)	// p23 I2C device Address
#define MC3413_I2C_ADDRESS_H			(0x6C)	// p23 I2C device Address

// State
#define MC3413_REG_OPCON			(0x07)	// p20 Mode State Register Address
#define MC3413_OPCON_WAKE			(0x01)	// p20 Mode State Wake [**** **01]
#define MC3413_OPCON_STANDBY		(0xFC)	// p20 Mode State Standby [**** **00]

// Sample Rate
#define MC3413_REG_SRTFR			(0x08)	// p33 SAMPLE RATE AND TAP FEATURE Register Address
#define MC3413_SRTFR_RATE			(0x09)	// p33 SAMPLE RATE 128Hz [0000 1001]
#define MC3413_SRTFR_RATE_OFF		(0xF0)	// p33 SAMPLE RATE OFF [1111 0000]

// Range Resolution
#define MC3413_REG_OUTCFG			(0x20)	// p37 OUTPUT CONFIGURATION Register Address

// XYZ_OUT
#define MC3413_REG_SR				(0x03)	// p29 Status Register Address
#define MC3413_ACQ_INT				(0x80)	// p29 Status ACQ_INT [1000 0000]
#define MC3413_REG_XYZOUT			(0x0D)	// p36 XYZ_OUT Register Address

// Product Code
#define MC3413_REG_PCODE			(0x3B)	// p44 Product Code Register Address
#define MC3413_WHOAMI_ID			(0x10)	// p44 Product Code [0001 0000]
#define MC3413_WHOAMI_CHECKID		(0xF1)	// p44 Product Code check [0001 ***0]&[1111 0001]->[0001 0000]

#endif


