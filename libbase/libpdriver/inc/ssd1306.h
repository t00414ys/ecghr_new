#ifndef __OLED_H__
#define __OLED_H__
#include "ssd1306_api.h"




/*
 *
 *		The rest of this is okay not change
 *
 */

// width : length = 7 : 3
extern const unsigned char battery[];

// width : length = 32 : 2
extern const unsigned char walk_man[];

// width : length = 32 : 2
extern const unsigned char footprint[];

// width : length = 32 : 2
extern const unsigned char tape[];

// width : length = 16 : 1
extern const unsigned char m_char[];

// width : length = 16 : 1
extern const unsigned char s_char[];

// width : length = 16 : 1
extern const unsigned char t_char[];

// width : length = 16 : 1
extern const unsigned char slash[];

// width : length = 14 : 1
extern const unsigned char bpm_char [];

// width : length = 11 : 1
extern const unsigned char hartlate[];

// width : length = 12 : 3
extern const unsigned char hyphen[];

// width : length = 12 : 3
extern const unsigned char haert[2][12];

// width : length = 12 : 3
extern const unsigned char number_dx[11][36];

// width : length = 24 : 5
//extern const unsigned char number_dx_big[11][120];

// width : length = 5 : 1
extern const unsigned char Ascii_1[97][5];

//	extern const unsigned char kuten_dode[];

extern const unsigned char gamen[];

extern const unsigned char font_x2_LUT[16];
extern const unsigned short font_x3_LUT[16];

//void oled_write_command(unsigned char command);
void oled_init( void );

void oled_fill_rect( int p_x, int p_y, int p_w, int p_l, unsigned char data );

/*! @brief		picture print to oled
	@param[in]	rate	rate of magnification
	@param[in]	p_x		drawing start point x(dot)
		@arg	0 to 127
	@param[in]	p_y		drawing start point y(line)
		@arg	0 to 7 (1line = 8dots)
	@param[in]	p_buf	picture data
	@return		void
*/
void oled_draw_picture( int rate, int p_x, int p_y, int p_w, int p_l, const unsigned char *p_buf );

/*! @brief		number print to oled
	@param[in]	rate	rate of magnification
	@param[in]	p_x		drawing start point x(dot)
		@arg	0 to 127
	@param[in]	p_y		drawing start point y(line)
		@arg	0 to 7 (1line = 8dots)
	@param[in]	digits	display digit
	@param[in]	input_num	input number
	@return		void
	@attention	aligning to the right. when if input number exceed the digits, print '-'
*/
void oled_num_print( int rate, int p_x, int p_y, int digits, unsigned int input_num );

/*! @brief		characters print to oled
	@param[in]	rate	rate of magnification
	@param[in]	p_x		drawing start point x(dot)
		@arg	0 to 127
	@param[in]	p_y		drawing start point y(line)
		@arg	0 to 7 (1line = 8dots)
	@param[in]	characters	input characters
		@arg	character code 0x20 to 0x
	@return		void
	@attention	please terminate characters a '\0' character.
	@attention	invalid character code convert to '?' character.
*/
void oled_char_print( int rate, int p_x, int p_y, char* characters );


/*! @brief		error display on the oled with error code.
	@param[in]	error_num	display error code
		@arg	0 to 999
	@return		void
*/
void oled_error_print( int error_num );

#endif
