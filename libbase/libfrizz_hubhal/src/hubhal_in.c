/*!******************************************************************************
 * @file hubhal_in.c
 * @brief source code for hubhal_in
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/tie/frizz_simd4w_que.h>
#include "hubhal_format.h"
#include "mes.h"
#include "hubhal_in.h"
#include "hubhal_out.h"
#include "hubhal_in_state.h"
#include "frizz_peri.h"
#include "frizz_env.h"
#include "frizz_util.h"

#if defined(USE_HOST_API_UART_IN)
#include "hubhal_host_api.h"
#endif

hubhal_in_info_t g_hubhal_in;

#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
/**
 * @brief Message register interrupt handler
 *
 * @param p command management information
 * @param data Message register content
 */
static void input_sub_irq( void* p, unsigned int data )
{
	hubhal_in_info_t *obj = ( hubhal_in_info_t* )p;
	int f_ack = 0;

#if	defined(USE_FIFO_BREAK_CODE)
	if( data == HUBHAL_FORMAT_BREAK_CODE ) {
		obj->head.w = data;
		obj->num = 0;
		obj->state = HUBHAL_IN_STATE_NONE;
		f_ack = 1;
	} else
#endif
	{
		if( obj->state == HUBHAL_IN_STATE_READING ) {
			obj->data[obj->num] = data;
			obj->num++;
			if( obj->head.num == obj->num ) {
				obj->state = HUBHAL_IN_STATE_CMD;
			}
			f_ack = 1;
		} else if( obj->state == HUBHAL_IN_STATE_NONE ) {
			obj->head.w = data;
			obj->num = 0;
			if( obj->head.prefix == HUBHAL_FORMAT_PREFIX
				&& obj->head.type == HUBHAL_FORMAT_TYPE_COMMAND ) {
				if( 0 < obj->head.num ) {
					obj->state = HUBHAL_IN_STATE_READING;
					f_ack = 1;
				}
			}
		}
	}
	hubhal_out_ack( f_ack );
	if( f_ack == 0 ) {
		obj->state = HUBHAL_IN_STATE_NONE;
	}
}
#endif	// !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)


/**
 * @brief initialize
 *
 * @param input type: #HUBHAL_IN_SOURCE
 */
void hubhal_in_init( HUBHAL_IN_SOURCE src )
{
	g_hubhal_in.state = HUBHAL_IN_STATE_NONE;
	g_hubhal_in.src = src;
	if( src == HUBHAL_IN_FROM_MES ) {
#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
		mes_init( input_sub_irq, &g_hubhal_in );
#endif
	}
}

/**
 * @brief get receiving command
 *
 * @param sen_id sensor ID
 * @param cmd_code CMD Code
 * @param num number of parameter
 * @param params parameter
 *
 * @return 0: command not exist, !0:command exist
 */
int hubhal_in_get_cmd( unsigned char *sen_id, unsigned int *cmd_code, unsigned int *num, void **params )
{
	int res = 0;
#if defined(USE_HOST_API_UART_IN)
	hubhal_host_uart_in();
#endif
	if( g_hubhal_in.src == HUBHAL_IN_FROM_QUEUE ) {
#if	!defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
		// for Debug
		if( iq_sensor_in_is_ready() == 0 ) {
			return res;
		} else {
			input_sub_irq( &g_hubhal_in, iq_sensor_in_pop() );
			while( g_hubhal_in.state == HUBHAL_IN_STATE_READING ) {
				input_sub_irq( &g_hubhal_in, iq_sensor_in_pop() );
			}
		}
#endif
	}
	if( g_hubhal_in.state == HUBHAL_IN_STATE_CMD ) {
		res = 1;
		*sen_id = g_hubhal_in.head.sen_id;
		*cmd_code = g_hubhal_in.data[0];
		*num = g_hubhal_in.num - 1;
		*params = &( g_hubhal_in.data[1] );
		g_hubhal_in.state = HUBHAL_IN_STATE_NONE;
	}
	return res;
}

#ifdef _DEBUG_
#include <stdio.h>
int main( void )
{
	input_init( HUBHAL_IN_FROM_QUEUE );
	while( 1 ) {
		unsigned int *params;
		unsigned int cmd_code, num;
		unsigned char sen_id;
		if( input_get_cmd( &sen_id, &cmd_code, &num, ( void** )&params ) != 0 ) {
			unsigned int i;
			printf( "sen_id:0x%02x, cmd_code:0x%08x, num:%d\r\n", ( unsigned int ) sen_id, ( unsigned int )  cmd_code, ( unsigned int )  num/*,(int) params*/ );
			for( i = 0; i < num; i++ ) {
				printf( "\t[%d]:0x%08x\r\n", ( int ) i, ( unsigned int ) params[i] );
			}
			input_res_cmd( sen_id, cmd_code, 0x12345678 );
		}
	}
	return 0;
}
#endif
