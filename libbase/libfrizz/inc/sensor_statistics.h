/*!******************************************************************************
 * @file    sensor_statistics.h
 * @brief   header for statistics of sensor data
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __SENSOR_STATISTICS_H__
#define __SENSOR_STATISTICS_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	frizz_fp4w		avg_vec;						///< current averaged vector
	frizz_fp4w		var_com;						///< current calculated variance of each component
	frizz_fp		pow_avg;						///< current power of averaged vector
	frizz_fp		var_abs;						///< current approximated variance for absolute value
} sensor_stat_data_t;

typedef struct {
	frizz_fp4w		*ring;							///< ring buffer
	frizz_fp4w		v_sum_vec;						///< summation of vector
	frizz_fp4w		v_sum_pow;						///< summation of power of each component
	frizz_fp		s_sum_abs;						///< summation of absolute
	frizz_fp		coeff_avg;						///< coefficient for average
	int				num;							///< ring buffer num
	int				idx;							///< ring buffer oldest index
	int				cnt_refresh;					///< count for refreshing
	int				f_available;					///< buffer status
} sensor_stat_buff_t;

/**
 * @brief information for sensor statistics module
 */
typedef struct {
	sensor_stat_buff_t	buff;						///< buffer for statistics
	sensor_stat_data_t	data;						///< statistics data
} sensor_stat_t;

/**
 * @brief initilize sensor_stat_t
 *
 * @param [in][out] h #sensor_stat_t
 * @param [in] buff ring buffer to calculate statistics value
 * @param [in] num ring buffer num
 *
 * @return 0: success, !0:not success
 */
int sensor_stat_init( sensor_stat_t *h, frizz_fp4w *buff, int num );

/**
 * @brief get buffer status
 *
 * @param [in] h #sensor_stat_t
 *
 * @return 0: not available, !0:available
 */
static inline int sensor_stat_is_available( const sensor_stat_t *h )
{
	return h->buff.f_available;
}

/**
 * @brief push sensor data
 *
 * @param [in][out] h #sensor_stat_t
 * @param [in] d sensor data
 *
 * @return buffer status
 */
int sensor_stat_push_data( sensor_stat_t *h, const frizz_fp4w d );

/**
 * @brief get buffered data
 *
 * @param [in][out]	h #sensor_stat_t
 * @param [in] idx	index for reference: 0:current, -1: one before, -2: two before ...
 *
 * @return indexed data
 *
 * @note if h->buff.num <= abs(idx) or 0 < idx: is not warranty
 */
static inline frizz_fp4w sensor_stat_get_buffer( const sensor_stat_t *h, int idx )
{
	idx += h->buff.idx - 1;
	if( idx < 0 ) {
		idx += h->buff.num;
	}
	return h->buff.ring[idx];
}

/**
 * @brief get current statistics data
 *
 * @param [in] h #sensor_stat_t
 *
 * @return statistics data #sensor_stat_data_t
 *
 * @note #sensor_stat_is_available() == 0: is not warranty
 */
static inline const sensor_stat_data_t* sensor_stat_get_data( const sensor_stat_t *h )
{
	return &( h->data );
}

#ifdef __cplusplus
}
#endif

#endif
