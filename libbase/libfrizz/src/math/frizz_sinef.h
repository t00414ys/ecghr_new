/*!******************************************************************************
 * @file frizz_sinef.h
 * @brief frizz_sinef() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief common function for sine / cosine
 *
 * @param [in] x
 * @param [in] cosine 0: sine, 1: cosine
 *
 * @return sine or cosine of x
 */
frizz_fp frizz_sinef( frizz_fp x, int cosine );

#ifdef __cplusplus
}
#endif
