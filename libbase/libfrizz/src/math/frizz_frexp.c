/*!******************************************************************************
 * @file frizz_frexp.c
 * @brief frizz_frexp() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

/**
 * @brief get significand and exponent
 * x = significand * 2^exp
 *
 * @param [in] x value
 * @param [out] exp exponent
 *
 * @return significand value [0.5, 1.0)
 */
frizz_fp frizz_frexp( frizz_fp x, int *exp )
{
	volatile _MathFPCast f;
	f.zp = x;
	*exp = 0;
	if( ( f.ui & 0x7FFFFFFF ) != 0 ) {
		if( f.fbit.exp == 0 ) {
			f.zp *= FRIZZ_MATH_TWO25;
			*exp = -25;
		}
		*exp += f.fbit.exp - 126;

		/* Get the mantissa. */
		f.ui = ( f.ui & 0x807fffff ) | 0x3f000000;
	}
	return f.zp;
}

