/*!******************************************************************************
 * @file frizz_asin.c
 * @brief frizz_asin() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_asinef.h"
#include "frizz_math.h"

/**
 * @brief arc sine
 *
 * @param [in] x [-1, +1]
 *
 * @return arc sine of x in radian, [-pi/2, +pi/2]
 */
frizz_fp frizz_asin( frizz_fp x )
{
	return frizz_asinef( x, 0 );
}

