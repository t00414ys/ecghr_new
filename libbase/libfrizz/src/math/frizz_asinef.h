/*!******************************************************************************
 * @file frizz_asinef.c
 * @brief frizz_asinef() function header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief arc sine / cosine
 *
 * @param [in] x
 * @param [in] acosine 0: arc sine, 1:arc cosine
 *
 * @return arc sine or cosine
 */
frizz_fp frizz_asinef( frizz_fp x, int acosine );


#ifdef __cplusplus
}
#endif
