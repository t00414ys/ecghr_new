/*!******************************************************************************
 * @file timer.h
 * @brief header for timer driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __TIMER_H__
#define __TIMER_H__
/** @addtogroup TIMER
 */
/** @ingroup TIMER */
/* @{ */

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief Callback function called when timer interrupt occurred.
 *
 * @param [in] args
 * @param [in] count no
 * @return void
*/
typedef void ( *timer_callback )( void*, int );

/**
 * @brief	Initialize timer and register callback function corresponds to specified timer.<BR>
 * 			If specified timer has started, stop the timer.<BR>
 * 			This timer driver uses xtensa Core internal timer(Timer0 - 2).
 *
 * @param [in] no	Timer number(0 - 2)
 * @param [in] pf	Pointer to callback function called when timer interrupt occurred.
 * @param [in] arg	Callback parameter
 * @return 0:Success,-1:Fail
 */
int timer_init( int no, timer_callback pf, void* arg );

/**
 * @brief	Start timer with specified period.<BR>
 *			Timer interrupt occurs at specified period and callback function registered by timer_init() is called.
 *
 * @param [in] no	Timer number(0 - 2)
 * @param [in]	core_freq	frizz core clock frequency
 *							When use external OSC as frizz core clock, set the frequency of the external OSC.
 *							When use internal OSC as frizz core clock, set 40MHz.
 * @param [in] msec			Timer period(msec)
 * @return 0:Success,-1:Fail
*/
int timer_start( int no, unsigned int core_freq, unsigned int msec );

/**
 * @brief	Stop timer.<BR>
 *			Next time to start timer, there is no need to call timer_init() again.
 *
 * @return 0:Success,-1:Fail
*/
int timer_stop( int no );

/**
 * @brief	Wait for timer interrupt with busy loop.<BR>
 *			This function is blocked until timer interrupt occurs.<BR>
 *			After timer interruption, timer interrupt count is returned.<BR>
 *			This interrupt count is cleared at timer starts or stops, and is incremented when timer interrupt occurred.
 *
 * @return Timer interrupt count
*/
unsigned int timer_wait( int no );

/**
 * @brief	Wait for timer interrupt with busy loop.<BR>
 *			This function is used Timer2<BR>
 *			This function is blocked until timer interrupt occurs.<BR>
 *			After timer interruption, timer interrupt count is returned.<BR>
 *			This interrupt count is cleared at timer starts or stops, and is incremented when timer interrupt occurred.
 *
 * @return void
*/
void mdelay( unsigned int msec );

//20171024�ǉ�
void udelay( unsigned int msec );


/**
 * @brief time valueu
 */
typedef struct {
	unsigned long	sec;
	unsigned long	nsec;
} frizz_timeval;

/**
 * @brief Measure the time
 *
 * @param [in] start clock count
 *
 * @return This function returns if A is 0 B.
 *         Unless B is zero, it returns in the ns time between A and B.
 */
unsigned long frizz_time_measure( unsigned long start );


/**
 * @brief Convert ns time to timeval
 *
 * @param [in] src  Pointer to put the results
 * @param [in] nsec source data
 *
 * @return This function returns if A is 0 B.
 *         Unless B is zero, it returns in the ns time between A and B.
 */
void frizz_time_to_timeval( frizz_timeval *src, unsigned long nsec );



/**
 * @brief get frizz time
 *
 * @param [in] sec 	sec pointer
 * @param [in] nsec nano sec pointer
 *
 * @return none
 */
void get_frizz_times( frizz_timeval *tv );


/**
 * @brief add frizz time
 *
 * @param [in] src source time data
 * @param [in] dst destination time data
 *
 * @return none
 *
 * @note src = src + dst
 */
void frizz_timevaladd( frizz_timeval *src, frizz_timeval *dst );


/**
 * @brief del frizz time
 *
 * @param [in] src source time data
 * @param [in] dst destination time data
 *
 * @return none
 *
 * @note src = src - dst
 */
void frizz_timevaldel( frizz_timeval *src, frizz_timeval *dst );


/**
 * @brief del frizz time
 *
 * @param [in] src source time data
 * @param [in] div a dividend.
 *
 * @return none
 *
 * @note src = src / div
 */
void frizz_timevaldiv( frizz_timeval *src, unsigned int div );



/**
 * @brief compare frizz time
 *
 * @param [in] src source time data
 * @param [in] dst destination time data
 *
 * @return if(src > dst) return 1; else if(src < dst) resturn -1; else return 0;
 */
int frizz_timevalcmp( frizz_timeval *src, frizz_timeval *dst );



#ifdef __cplusplus
}
#endif //__cplusplus

/* @} */
#endif // #ifndef __TIMER_H__
