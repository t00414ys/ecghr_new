/*!******************************************************************************
 * @file    spo2_driver.h
 * @brief   sample program for spo2 sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __SPO2_DRIVER_H__
#define __SPO2_DRIVER_H__

#include "config.h"
#include "base_driver.h"


#if defined(USE_ADPD142)
#include "adpd142_api.h"
#define	ADPD142_DATA	{	adpd142_spo2_init,		adpd142_spo2_ctrl,	adpd142_spo2_rcv ,	adpd142_spo2_conv, 	\
							adpd142_spo2_setparam,	adpd142_get_ver,	adpd142_get_name ,	0,				\
							{adpd142_get_condition,	0,					0,					0}				},
#else
#define	ADPD142_DATA
#endif


#endif /*  */
