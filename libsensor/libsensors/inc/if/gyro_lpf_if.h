/*!******************************************************************************
 * @file    gyro_lpf_if.h
 * @brief   virtual gyro lpf sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GYRO_LPF_IF_H__
#define __GYRO_LPF_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup GYRO_LPF GYRO LPF
 *  @{
 */
#define GYRO_LPF_ID	SENSOR_ID_GYRO_LPF	//!< A gyro lpf sensor interface ID


/**
 * @struct gyro_lpf_data_t
 * @brief Output data structure for gyro lpf sensor
 */
typedef struct {
	FLOAT		data[3];	//!< Angular velocity vector [rad/s] data[0]:X-axis Component[rad/s], data[1]:Y-axis Component[rad/s], data[2]:Z-axis Component[rad/s]
	FLOAT		degree;	//!< degreeC
} gyro_lpf_data_t;
//@}

/**
 * @name Command List
 */
//@{
/**
 * @brief Set sensor parameter
 *
 * @param cmd_code SET_DRIFT_CORRECTION_PARAM
 * @param param[0] frizz_fp x-axis drift value
 * @param param[1] frizz_fp y-axis drift value
 * @param param[2] frizz_fp z-axis drift value
 * @param param[3] frizz_fp degreeC
 *
 * @return 0: OK, -1: Command Error
 */
#define	SET_DRIFT_CORRECTION_PARAM			0x00
//@}

#endif
