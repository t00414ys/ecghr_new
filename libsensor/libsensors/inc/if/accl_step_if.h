/*!******************************************************************************
 * @file    accl_step_if.h
 * @brief   virtual accel step detecotr sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_STEP_IF_H__
#define __ACCL_STEP_IF_H__

#include "libsensors_id.h"

/** @defgroup ACCEL_STEP_DETECTOR ACCEL STEP DETECTOR
 *  @{
 */
#define ACCEL_STEP_DETECTOR_ID SENSOR_ID_ACCEL_STEP_DETECTOR	//!< An accel step detector sensor interface ID

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
