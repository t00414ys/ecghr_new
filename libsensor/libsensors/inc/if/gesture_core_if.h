/*!******************************************************************************
 * @file    gesture_core_if.h
 * @brief   virtual gesture core interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GESTURE_CORE_IF_H__
#define __GESTURE_CORE_IF_H__

#include "libsensors_id.h"

/** @defgroup GESTURE GESTURE
 *  @{
 */
#define GESTURE_ID	SENSOR_ID_GESTURE	//!< gesture core interface ID

//@}
/** @} */
#endif
