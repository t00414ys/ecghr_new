/*!******************************************************************************
 * @file    pdr_geofencing_if.h
 * @brief   virtual pdr geofencing sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PDR_GEOFENCING_IF_H__
#define __PDR_GEOFENCING_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup PDR_GEOFENCING PDR GEOFENCING
 *  @{
 */
#define PDR_GEOFENCING_ID	SENSOR_ID_PDR_GEOFENCING	//!< pdr geofencing sensor interface ID

/**
 * @enum PDR_GEOFENCING_STATUS
 * @brief A kind of area
 */
typedef enum {
	PDR_GEOFENCING_ST_IN_SAFE_ZONE = 0,	//!< in Safe Zone
	PDR_GEOFENCING_ST_IN_WARNING_ZONE,	//!< in Warning Zone
	PDR_GEOFENCING_ST_ALARM,			//!< Alarming
} PDR_GEOFENCING_STATUS;

/**
 * @struct pdr_geofencing_data_t
 * @brief Output data structure for pdr geofencing sensor
 */
typedef struct {
	PDR_GEOFENCING_STATUS 	status;		//!< Current Status
	FLOAT					rpos[2];	//!< local location (0:x, 1:y) [m]
	int						time;		//!< current time in outside of Safe Zone [sec]
} pdr_geofencing_data_t;

/**
 * @name Command List
 */
//@{

/**
 * @brief Set Parameter for Geofencing
 * @param param[0] int: Radius of Safe Zone [m] (0~)
 * @param param[1] int: Radius of Warning Zone [m] (0~)
 * @param param[2] int: duration in Warning Zone [sec] (0~86400)
 * @param param[3] int: Time of length to out of warning zone from safe zone [sec] (0~86400)
 *
 * @return 0: Sucess, -1: Parameter Error, -2: Anything Error
 * @brief parameter restriction: param[0] < param[1]
 */
#define PDR_GEOFENCING_CMD_SET_PARAMETER		0x00

/**
 * @brief Reset start point
 * @return 0: Sucess
 */
#define PDR_GEOFENCING_CMD_RESET_START_POINT		0x01

/**
 * @brief Set Length of Stride
 * @param param[0] unsigned int length of step's stride [cm] (0~200)
 * @return 0: Sucess, -1: Parameter Error, -2: Anything Error
 */
#define PDR_GEOFENCING_CMD_SET_STEP_STRIDE		0x02

/**
 * @brief Get Version
 * @return version
 */
#define PDR_GEOFENCING_CMD_GET_VERSION		0xff
//@}

/** @} */
#endif
