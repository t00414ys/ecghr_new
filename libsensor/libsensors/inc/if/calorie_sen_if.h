/*!******************************************************************************
 * @file    calorie_sen_if.h
 * @brief   virtual calorie sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __CALORIE_SEN_IF_H__
#define __CALORIE_SEN_IF_H__

#include "libsensors_id.h"

/**
 * @brief motion information structure
 */
typedef struct {
	unsigned int		reset_status;		//!< reset status 1:reset 0:not reset

	float				height;				//!< unit: m
	unsigned int		weight;				//!< unit: kg

	unsigned int		step_freq_s;		//!< unit: step/second
	unsigned int		step_freq_m;		//!< unit: step/minute
} calorie_info_t;

/** @defgroup CALORIE_SEN CALORIE
 *  @{
 */
#define CALORIE_ID SENSOR_ID_CALORIE	//!< An calorie interface ID

/**
 * @name Command List
 */
//@{

/**
 * @brief	Crear sensor data
 * @param	cmd_code	CALORIE_SENSOR_CLEAN
 * @return	0:OK other:NG
 */
#define CALORIE_SENSOR_CLEAN			(0x00)

/**
 * @brief	Set infomation
 * @param	cmd_code	CALORIE_SENSOR_SET_INFO
 * @param param[0] int height [CM]
 * @param param[1] int weight [KG]
 * @return	0:OK other:NG
 */
#define CALORIE_SENSOR_SET_INFO			(0x01)
//@}

/** @} */
#endif
