/*!******************************************************************************
 * @file    group_gyro.h
 * @brief   group of virtual gyro sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief gyro_hpf, gyro_lpf and gyro_uncalib sensor header group
 */

#ifndef __GROUP_GYRO_H__
#define __GROUP_GYRO_H__

#include "gyro_hpf.h"
#include "gyro_lpf.h"
#include "gyro_uncalib.h"
#include "gyro_posture.h"
#endif
