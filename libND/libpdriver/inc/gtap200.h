/*!******************************************************************************
 * @file    gtap200.h
 * @brief   gtap200 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GTAP200_H__
#define __GTAP200_H__
#include "gtap200_api.h"

/*
 *
 *		The rest of this is okay not change
 *
 */

#define PRES_GTAP_I2C_ADDRESS			(0x76)		///< Pressure Sensor(made from GoerTek)
#define GTAP200_REG_CMD					0xAC
#define	MEASURE_CMD						0x01


#endif



