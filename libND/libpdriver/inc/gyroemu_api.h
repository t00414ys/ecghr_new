/*!******************************************************************************
 * @file    gyroemu_api.h
 * @brief   sample program for accel/gyro sensor emulation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GYROEMU_API_H__
#define __GYROEMU_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

//
int  gyroemu_init( unsigned int param );
void gyroemu_ctrl_gyro( int f_ena );
unsigned int gyroemu_rcv_gyro( unsigned int tick );
int  gyroemu_conv_gyro( frizz_fp data[4] );

int gyroemu_setparam_gyro( void *ptr );
unsigned int gyroemu_get_ver( void );
unsigned int gyroemu_get_name( void );

int gyroemu_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif	/* __GYROEMU_API_H__ */
