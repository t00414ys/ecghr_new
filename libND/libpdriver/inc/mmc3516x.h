/*!******************************************************************************
 * @file    mmc3516x.h
 * @brief   mmc3516x sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MMC3516X_H__
#define __MMC3516X_H__
#include "mmc3516x_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */

/*
 * This is use magnet calibration
 *(DEFINITED) : Enable  the calibration process maker create
 *(~DEFINITED): Disable the calibration process maker create
 */
//#define	D_USE_CALIB_MMC3516
//#define	D_USE_AYAME_EXTRAPARAM



/*
 *
 *		The rest of this is okay not change
 *
 */

// Constant
// Output [uT]
#define MMC3516X_SCALE_PER_LSB		((float)100.0)

#define MMC3516X_I2C_ADDRESS		0x30
#define MMC3516X_WHOAMI_ID			0x06

#define MMC3516X_REG_CTRL0			0x07	// Control register 0
#define MMC3516X_REG_CTRL1			0x08	// Control register 1
#define MMC3516X_REG_DS				0x06	// Device status
#define MMC3516X_REG_PRODUCTID_1	0x20	// Product ID

#define MMC3516X_REG_XL				0x00	// Xout Low
#define MMC3516X_REG_XH				0x01	// Xout High
#define MMC3516X_REG_YL				0x02	// Yout Low
#define MMC3516X_REG_YH				0x03	// Yout High
#define MMC3516X_REG_ZL				0x04	// Zout Low
#define MMC3516X_REG_ZH				0x05	// Zout High

#endif
