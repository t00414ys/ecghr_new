/*!******************************************************************************
 * @file    lt1ph03_api.h
 * @brief   lt1ph03 sensor api header
 * @par     copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __LT_1PH03_API_H__
#define __LT_1PH03_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int lt1ph03_init( unsigned int param );
void lt1ph03_ctrl( int f_ena );
unsigned int lt1ph03_rcv( unsigned int tick );
int lt1ph03_conv( frizz_fp *data );
int lt1ph03_setparam( void *ptr );
unsigned int lt1ph03_get_ver( void );
unsigned int lt1ph03_get_name( void );

int lt1ph03_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif // __LT_1PH03_API_H__

