/*!******************************************************************************
 * @file    stk8313_api.h
 * @brief   stk8313 sensor api header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __STK8313_API_H__
#define __STK8313_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int stk8313_init( unsigned int param );
void stk8313_ctrl_accl( int f_ena );
unsigned int stk8313_rcv_accl( unsigned int tick );
int stk8313_conv_accl( frizz_fp data[3] );
int stk8313_setparam_accl( void *ptr );
unsigned int stk8313_get_ver( void );
unsigned int stk8313_get_name( void );
unsigned char stk8313_get_addr( void );

int stk8313_accl_get_condition( void *data );
int stk8313_accl_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif

#endif
