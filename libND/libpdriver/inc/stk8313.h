/*!******************************************************************************
 * @file    stk8313.h
 * @brief   stk8313 sensor driver header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __STK8313_H__
#define __STK8313_H__
#include "stk8313_api.h"

/*
 * This is use accel sensor range (To select either one)
 *(DEFINITED) : Enable  use accl sensor range
 *(~DEFINITED): Disable  use accl sensor range
 */
//#define		ACC_2G_RANGE
//#define		ACC_4G_RANGE
#define		ACC_8G_RANGE

#if defined(ACC_2G_RANGE)
#define STK8313_ACCEL_ACC_RANGE		STK8313_ACC_2G
#define STK8313_ACCEL_PER_LSB			(1./256.)
#endif
#if defined(ACC_4G_RANGE)
#define STK8313_ACCEL_ACC_RANGE		STK8313_ACC_4G
#define STK8313_ACCEL_PER_LSB			(1./256.)
#endif
#if defined(ACC_8G_RANGE)
#define STK8313_ACCEL_ACC_RANGE		STK8313_ACC_8G
#define STK8313_ACCEL_PER_LSB			(1./256.)
#endif

#define STK8313_I2C_ADDRESS     0x22

//Registers
#define	STK8313_XOUT	0x00
#define	STK8313_YOUT	0x02
#define	STK8313_ZOUT	0x04
#define	STK8313_TILT	0x06	/* Tilt Status */
#define	STK8313_SRST	0x07	/* Sampling Rate Status */
#define	STK8313_SPCNT	0x08	/* Sleep Count */
#define	STK8313_INTSU	0x09	/* Interrupt setup*/
#define	STK8313_MODE	0x0A
#define	STK8313_SR		0x0B	/* Sample rate */
#define	STK8313_PDET	0x0C	/* Tap Detection */
#define	STK8313_DEVID	0x0E	/* Device ID */
#define	STK8313_OFSX	0x0F	/* X-Axis offset */
#define	STK8313_OFSY	0x10	/* Y-Axis offset */
#define	STK8313_OFSZ	0x11	/* Z-Axis offset */
#define	STK8313_PLAT	0x12	/* Tap Latency */
#define	STK8313_PWIN	0x13	/* Tap Window */
#define	STK8313_FTH		0x14	/* Free-Fall Threshold */
#define	STK8313_FTM	    0x15	/* Free-Fall Time */
#define	STK8313_STH	    0x16	/* Shake Threshold */
#define	STK8313_ISTMP	0x17	/* Interrupt Setup */
#define STK8313_INTMAP	0x18	/* Interrupt Map */
#define	STK8313_RESET	0x20	/* software reset */

#define STK8313_ANALOG_FRONT_END  0x24
#define STK8313_OTP_ADDRESS       0x3D
#define STK8313_OTP_DATA          0x3E
#define STK8313_OTP_CONTROL       0x3F

// ACC_MODE masks
#define STK8313_ACC_MODE_MASK    0xFE
#define STK8313_ACC_STANDBY_MODE 0x00
#define STK8313_ACC_ACTIVE_MODE  0x01

// ODR masks
#define STK8313_ODR             100
#define STK8313_ODR_400Hz		0x00
#define STK8313_ODR_200Hz		0x01
#define STK8313_ODR_100Hz		0x02
#define STK8313_ODR_50Hz		0x03
#define STK8313_ODR_25Hz		0x04
#define STK8313_ODR_12_5Hz		0x05
#define STK8313_ODR_6_25Hz		0x06
#define STK8313_ODR_3_125Hz		0x07

#define	STK8313_ACC_MASK        0x3F
#define	STK8313_ACC_2G		    (0x0<<6)
#define	STK8313_ACC_4G		    (0x1<<6)
#define	STK8313_ACC_8G		    (0x2<<6)

// STH masks
#define	STK8313_STH_MASK        0xF8
#define	STK8313_STH_1_125G		0x00
#define	STK8313_STH_1_250G		0x01
#define	STK8313_STH_1_375G      0x02
#define	STK8313_STH_1_500G      0x03
#define	STK8313_STH_1_625G      0x04
#define	STK8313_STH_1_750G      0x05
#define	STK8313_STH_1_875G      0x06
#define	STK8313_STH_2_000G      0x07

#endif // __STK8313_H__

